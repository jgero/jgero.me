FROM docker.io/node:16-alpine AS webcomp-builder

WORKDIR /app

COPY package.json .
COPY package-lock.json .
RUN npm install

COPY vite.config.js .
COPY webcomponents webcomponents
RUN npm run build

FROM docker.io/golang:1.16.4 AS go-builder

WORKDIR /app

COPY go.mod .
COPY go.sum .
RUN go mod download

COPY assets assets
RUN cp "$(go env GOROOT)/misc/wasm/wasm_exec.js" assets/wasm_exec.js
COPY --from=webcomp-builder /app/assets/webcomponents.es.js assets/webcomponents.es.js
COPY wasm wasm
RUN GOOS=js GOARCH=wasm go build -ldflags="-w -s" -o assets/hello.wasm wasm/main.go

COPY webserver webserver
COPY main.go .
RUN CGO_ENABLED=0  GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" -o /out/webserver main.go

FROM scratch

ENV SERVER_PORT 8080

# copy certificates to all standard authorities from go-builder
COPY --from=go-builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/

COPY --from=go-builder /out/webserver /app/webserver
ENTRYPOINT ["/app/webserver"]
