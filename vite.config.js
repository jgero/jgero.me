import { defineConfig } from 'vite'
import { svelte } from '@sveltejs/vite-plugin-svelte'

export default defineConfig({
	build: {
		outDir: "./assets",
		emptyOutDir: false,
		lib: {
			entry: "./webcomponents/main.js",
			formats: [ "es" ],
		},
	},
	plugins: [
		svelte({
			compilerOptions: {
				customElement: true,
			}
		})
	],
})
