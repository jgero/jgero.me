# jgero.me webpage

This webpage is a mixture of some posts about things I do and projects I work on and a place for me
to experiment with different technologies I like and want to try.

## Stack

The pages are server side rendered Go templates with some WASM and Svelte sprinkled in. It will be
deployed as container.

## Development

[Reflex](https://github.com/cespare/reflex) can be used to automatically rebuild the container image
on file changes.
```bash
reflex -s -- bash -c 'podman build -t wasm -f build/webserver.Containerfile . && podman run -p 8000:8000 wasm'
```

