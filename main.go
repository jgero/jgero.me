package main

import (
	"net/http"

	"gitlab.com/jgero/jgero.me/webserver"
	"gitlab.com/jgero/jgero.me/assets"
)

func main() {
	l := webserver.NewConsoleLogger()
	serv := webserver.NewWebserver(":8000", l)

	m1 := func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			next.ServeHTTP(w, r)
		})
	}
	m2 := func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			next.ServeHTTP(w, r)
		})
	}

	serv.Use(m1)
	serv.Use(m2)

	a := assets.GetAssets()
	for _, asset := range a {
		serv.Register(asset.Path, http.FileServer(http.FS(asset.Val)).ServeHTTP)
	}

	// serv.Register("/hello", func(w http.ResponseWriter, r *http.Request) {
	// 	fmt.Println("writing response")
	// 	w.Write([]byte("hello world"))
	// })
	//
	serv.Finalize()
	serv.Start()
}
