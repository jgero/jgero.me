package webserver

import (
	"io"
	"log"
	"os"
)

// INFO -------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------
// The idea behind this interface is to make it possible to provide different logging targets while
// also enabling customization of how the messages are formatted. I want to avoid making the default
// logger mandatory. There is probably a better way of doing this, the current solution may be just
// temporary. Maybe I switch it to just pass some writers into a constructor function and always
// create loggers from these writers.
// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------
type WebLogger interface {
	Debug(s string)
	Debugf(s string, args ...interface{})
	DebugWriter() io.Writer
	Warn(s string)
	Warnf(s string, args ...interface{})
	WarnWriter() io.Writer
	Error(s string)
	Errorf(s string, args ...interface{})
	ErrorWriter() io.Writer
}

type ConsoleLogger struct {
	warningLogger *log.Logger
	debugLogger   *log.Logger
	errorLogger   *log.Logger
}

func NewConsoleLogger() WebLogger {
	return &ConsoleLogger{
		debugLogger :   log.New(os.Stdout, "[DEBUG]: ", log.LstdFlags),
		warningLogger : log.New(os.Stdout, "[WARNING]: ", log.LstdFlags),
		errorLogger :   log.New(os.Stdout, "[ERROR]: ", log.LstdFlags),
	}
}

func (l *ConsoleLogger) Debug(s string) {
	l.debugLogger.Println(s)
}
func (l *ConsoleLogger) Debugf(s string, args ...interface{}) {
	l.debugLogger.Printf(s, args...)
}
func (l *ConsoleLogger) DebugWriter() io.Writer {
	return l.debugLogger.Writer()
}

func (l *ConsoleLogger) Warn(s string) {
	l.warningLogger.Println(s)
}
func (l *ConsoleLogger) Warnf(s string, args ...interface{}) {
	l.warningLogger.Printf(s, args...)
}
func (l *ConsoleLogger) WarnWriter() io.Writer {
	return l.warningLogger.Writer()
}

func (l *ConsoleLogger) Error(s string) {
	l.errorLogger.Println(s)
}
func (l *ConsoleLogger) Errorf(s string, args ...interface{}) {
	l.errorLogger.Printf(s, args...)
}
func (l *ConsoleLogger) ErrorWriter() io.Writer {
	return l.errorLogger.Writer()
}
