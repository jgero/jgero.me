package webserver_test

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/jgero/jgero.me/webserver"
)

var helloResponder = func(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte(fmt.Sprintf("hello %s", r.URL.Path)))
}

type muxCase struct {
	def   string
	cases []muxCaseResult
}

type muxCaseResult struct {
	val        string
	shouldFail bool
}

func TestParams(t *testing.T) {
	mux := webserver.NewReqestMultiplexer()
	mux.Register("/a/:b/:c/d", func(w http.ResponseWriter, r *http.Request) {
		var params map[string]string
		var ok bool
		if params, ok = r.Context().Value(webserver.CONTEXT_PARAMS).(map[string]string); !ok {
			t.Fatalf("request params map is missing")
		}
		if params["b"] != "my" {
			t.Errorf("expected param b to be 'my' but is: %v", params["b"])
		}
		if params["c"] != "params" {
			t.Errorf("expected param c to be 'params' but is: %v", params["c"])
		}
	})
	rr := httptest.NewRecorder()
	req := httptest.NewRequest("GET", "/a/my/params/d", nil)
	mux.ServeHTTP(rr, req)
}

func TestMux(t *testing.T) {
	mux := webserver.NewReqestMultiplexer()
	cases := []muxCase{
		{def: "/", cases: []muxCaseResult{
			{val: "/", shouldFail: false},
		}},
		{def: "/hello", cases: []muxCaseResult{
			{val: "/hello", shouldFail: false},
		}},
		{def: "/hello/world", cases: []muxCaseResult{
			{val: "/hello/world", shouldFail: false},
		}},
		{def: "/foo/bar", cases: []muxCaseResult{
			{val: "/foo", shouldFail: true},
			{val: "/foo/bar", shouldFail: false},
		}},
		{def: "/my/:param", cases: []muxCaseResult{
			{val: "/my", shouldFail: true},
			{val: "/my/bar", shouldFail: false},
			{val: "/my/foo/", shouldFail: false},
		}},
		{def: "/a/:b/:c/d", cases: []muxCaseResult{
			{val: "/a/d", shouldFail: true},
			{val: "/a/b/c/d", shouldFail: false},
			{val: "/a/a/a/d", shouldFail: false},
			{val: "/a/a/a/a", shouldFail: true},
			{val: "/a/a/a", shouldFail: true},
		}},
	}
	for _, route := range cases {
		t.Run(fmt.Sprintf("register '%s'", route.def), func(t *testing.T) {
			err := mux.Register(route.def, helloResponder)
			if err != nil {
				t.Errorf("could not register route %s because: %s", route.def, err)
			}
		})
	}

	for _, route := range cases {
		for _, c := range route.cases {
			t.Run(fmt.Sprintf("run case '%s' on def '%s'", c.val, route.def), func(t *testing.T) {
				rr := httptest.NewRecorder()
				req := httptest.NewRequest("GET", c.val, nil)
				mux.ServeHTTP(rr, req)
				res := rr.Result()
				defer res.Body.Close()
				data, err := ioutil.ReadAll(res.Body)
				if err != nil {
					t.Errorf("could not read response body: %s", err)
				}
				is := string(data)
				t.Logf("response is: '%s'", is)
				expected := fmt.Sprintf("hello %s", c.val)
				if is != expected && !c.shouldFail {
					t.Errorf("unexpected response, expected: '%s'", expected)
				}
			})
		}
	}
}
