package webserver

import (
	"compress/gzip"
	"context"
	"io"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"sync"
	"time"

	"github.com/google/uuid"
)

const REQUEST_ID_KEY = "X-Request-Id"

type routedWebserver struct {
	server *http.Server
	logger		WebLogger
	isHealthy   bool
	healthMutex *sync.Mutex
	done        chan bool
	quit        chan os.Signal
	mux         RequestMultiplexer
	m           []Middleware
}
type Middleware = func(n http.Handler) http.Handler

type Webserver interface {
	Use(m Middleware)
	Register(route string, f http.HandlerFunc) error
	Finalize()
	Start()
}

func NewWebserver(port string, l WebLogger) Webserver {
	s := &routedWebserver{
		server: &http.Server{
			Addr:         port,
			ErrorLog:     log.New(l.ErrorWriter(), "[WEBSERVER] ", 0),
			ReadTimeout:  5 * time.Second,
			WriteTimeout: 10 * time.Second,
			IdleTimeout:  15 * time.Second,
		},
		logger: l,
		isHealthy:   false,
		healthMutex: &sync.Mutex{},
		done: make(chan bool),
		quit: make(chan os.Signal, 1),
		mux:         NewReqestMultiplexer(),
		m:           make([]Middleware, 0),
	}
	signal.Notify(s.quit, os.Interrupt)
	s.Use(s.loggingMiddleware())
	s.Use(s.zipMiddleware())
	return s
}

func (s *routedWebserver) Finalize() {
	s.mux.Register("/health", s.health())
	s.server.Handler = s.mux
	for i := len(s.m) - 1; i >= 0; i-- {
		s.server.Handler = s.m[i](s.server.Handler)
	}
}

func (s *routedWebserver) Start() {
	s.logger.Debug("server is starting")
	go s.waitForShutdown()
	s.healthMutex.Lock()
	s.isHealthy = true
	s.healthMutex.Unlock()
	s.logger.Debugf("server is about to be ready to handle requests at %s", s.server.Addr)
	if err := s.server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		s.logger.Errorf("could not listen on %s: %s", s.server.Addr, err)
		os.Exit(1)
	}
	<-s.done
	s.logger.Debug("server stopped")
}

func (s *routedWebserver) waitForShutdown() {
	<-s.quit
	s.logger.Debug("server is shutting down")
	s.healthMutex.Lock()
	s.isHealthy = false
	s.healthMutex.Unlock()
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	s.server.SetKeepAlivesEnabled(false)
	if err := s.server.Shutdown(ctx); err != nil {
		s.logger.Errorf("could not shut server down gracefully: %s", err)
	}
	close(s.done)
}

func (s *routedWebserver) Use(m Middleware) {
	s.m = append(s.m, m)
}

func (s *routedWebserver) Register(route string, f http.HandlerFunc) error {
	return s.mux.Register(route, f)
}

func (s *routedWebserver) health() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		s.healthMutex.Lock()
		isHealthy := s.isHealthy
		s.healthMutex.Unlock()
		if isHealthy {
			w.WriteHeader(http.StatusNoContent)
		} else {
			w.WriteHeader(http.StatusServiceUnavailable)
		}
	}
}

func (s *routedWebserver) loggingMiddleware() Middleware {
	return func(n http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			startTime := time.Now()
			requestId := r.Header.Get(REQUEST_ID_KEY)
			if requestId == "" {
				requestId = uuid.New().String()
			}
			ctx := context.WithValue(r.Context(), REQUEST_ID_KEY, requestId)
			w.Header().Set(REQUEST_ID_KEY, requestId)
			defer func() {
				s.logger.Debugf("ID: %s %s %s responded in %d ms", requestId, r.Method, r.URL.Path, time.Since(startTime).Milliseconds())
			}()
			n.ServeHTTP(w, r.WithContext(ctx))
		})
	}
}

type gzipResponseWriter struct {
	io.Writer
	http.ResponseWriter
}

func (w gzipResponseWriter) Write(b []byte) (int, error) {
	return w.Writer.Write(b)
}

func (s *routedWebserver) zipMiddleware() Middleware {
	return func(n http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if !strings.Contains(r.Header.Get("Accept-Encoding"), "gzip") {
				n.ServeHTTP(w, r)
				return
			}
			w.Header().Set("Content-Encoding", "gzip")
			gz := gzip.NewWriter(w)
			defer gz.Close()
			gzrw := gzipResponseWriter{Writer: gz, ResponseWriter: w}
			n.ServeHTTP(gzrw, r)
		})
	}
}
