package webserver

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"strings"
)

const CONTEXT_PARAMS = "params"

type mult struct {
	trie         routeTrie
	errorHandler func(w http.ResponseWriter, r *http.Request, err error)
}

type RequestMultiplexer interface {
	ServeHTTP(w http.ResponseWriter, r *http.Request)
	Register(route string, f http.HandlerFunc) error
}

func NewReqestMultiplexer() RequestMultiplexer {
	return &mult{
		trie: routeTrie{},
		errorHandler: func(w http.ResponseWriter, r *http.Request, err error) {
			w.Header().Add("Content-Type", "application/json")
			w.WriteHeader(http.StatusNotFound)
			w.Write([]byte(fmt.Sprintf("{\"error\": \"%s\"}", err)))
		},
	}
}

func (mux *mult) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	v, params, err := mux.trie.search(r.URL.Path)
	if err == nil {
		r = r.WithContext(context.WithValue(r.Context(), CONTEXT_PARAMS, params))
		v(w, r)
	} else if errors.Is(err, &MissingRouteError{}) {
		mux.errorHandler(w, r, err)
	} else {
		panic(fmt.Errorf("bad error when getting route from trie: %w", err))
	}
}

func (mux *mult) Register(route string, f http.HandlerFunc) error {
	return mux.trie.insert(route, f)
}

// ------------------------------------------------------------------------------------------------
// Trie (prefix tree) implementaion
// ------------------------------------------------------------------------------------------------

type routeTrie struct {
	root *node
}

type MissingRouteError struct {
	route string
}

func (err *MissingRouteError) Error() string {
	return fmt.Sprintf("no entry for route '%s'", err.route)
}

func (err *MissingRouteError) Is(target error) bool {
	switch target.(type) {
	case *MissingRouteError:
		return true
	}
	return false
}

// search for a route in the trie and get the handler back
//
// returns the value and a param map or an error if there is no matching route
func (x *routeTrie) search(key string) (http.HandlerFunc, map[string]string, error) {
	paramMap := make(map[string]string, 0)
	if !strings.HasPrefix(key, "/") {
		return nil, nil, fmt.Errorf("cannot insert route %s: prefix '/' is missing", key)
	}
	// trailing slashes are trimmed to avoid ambiguity
	if len(key) > 1 {
		key = strings.TrimSuffix(key, "/")
	}
	segments := strings.Split(key, "/")[1:]
	currentNode := x.root
	for _, segment := range segments {
		if currentNode.children[segment] == nil {
			if currentNode.children[":"] == nil {
				return nil, nil, &MissingRouteError{route: key}
			}
			paramMap[currentNode.children[":"].parameter] = segment
			segment = ":"
		}
		currentNode = currentNode.children[segment]
	}
	if !currentNode.isTerminal {
		return nil, nil, &MissingRouteError{route: key}
	}
	return currentNode.value, paramMap, nil
}

// insert route with handler into the trie
//
// the route needs to have a leading "/"
func (x *routeTrie) insert(key string, value http.HandlerFunc) error {
	if !strings.HasPrefix(key, "/") {
		return fmt.Errorf("cannot insert route %s: prefix '/' is missing", key)
	}
	// trailing slashes are trimmed to avoid ambiguity
	if len(key) > 1 {
		key = strings.TrimSuffix(key, "/")
	}
	if x.root == nil {
		x.root = newNode(nil)
	}
	segments := strings.Split(key, "/")[1:]
	currentNode := x.root
	for _, segment := range segments {
		index := segment
		var parameter string
		if strings.HasPrefix(segment, ":") {
			if len(segment) < 2 {
				return fmt.Errorf("cannot insert route %s: parameter in segment %s needs to be named", key, segment)
			}
			index = ":"
			parameter = strings.TrimPrefix(segment, ":")
		}
		if currentNode.children[index] == nil {
			currentNode.children[index] = newNode(nil)
		}
		if parameter != "" {
			currentNode.children[index].parameter = parameter
		}
		currentNode = currentNode.children[index]
	}
	currentNode.value = value
	currentNode.isTerminal = true
	return nil
}

type node struct {
	isTerminal bool
	parameter  string
	children   map[string]*node
	value      func(w http.ResponseWriter, r *http.Request)
}

func newNode(value http.HandlerFunc) *node {
	return &node{
		isTerminal: false,
		children:   make(map[string]*node),
		value:      value,
	}
}
