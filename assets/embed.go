package assets

import (
	"embed"
	"io/fs"
)

//go:embed index.html
var index embed.FS
//go:embed hello.wasm
var helloWasm embed.FS
//go:embed wasm_exec.js
var glue embed.FS
//go:embed webcomponents.es.js
var webcomponents embed.FS

type Asset struct {
	Val  fs.FS
	Path string
}

func GetAssets() []Asset {
	return []Asset{
		{Val: index, Path: "/"},
		{Val: helloWasm, Path: "/hello.wasm"},
		{Val: glue, Path: "/wasm_exec.js"},
		{Val: webcomponents, Path: "/webcomponents.es.js"},
	}
}
